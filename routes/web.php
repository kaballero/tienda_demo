<?php

use App\Http\Controllers\Api\AuthenticationController;
use App\Http\Controllers\EcommerceController;
use App\Http\Controllers\site\ProductController as ProductSiteController;
use App\Models\Products;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::match(['get'], '/', [EcommerceController::class, 'index' ]);
Route::match(['get'], 'product/{product_slug}', [EcommerceController::class, 'detail_product' ]);

Route::match(['get'], 'login', [AuthenticationController::class, 'login' ] );
Route::match(['get'], 'registro', [AuthenticationController::class, 'registro' ] );

Route::match(['get'], 'dashboard', [ProductSiteController::class, 'index' ] );
Route::match(['get'], 'admin/product/add', [ProductSiteController::class, 'add_product' ] );
Route::match(['get'], 'admin/product/edit/{id}', [ProductSiteController::class, 'add_product' ] );


