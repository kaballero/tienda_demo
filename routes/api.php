<?php

use App\Http\Controllers\Api\AuthenticationController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
/*
Route::match(['get'], '/products', [ProductController::class, 'index' ]);
Route::match(['get'], '/products/{slug}', [ProductController::class, 'index' ]);*/

Route::match(['get'], 'categories/list', [CategoryController::class, 'index' ]);
Route::match(['get'], 'products/list', [ProductController::class, 'list' ]);
Route::match(['get'], 'product/{slug}', [ProductController::class, 'show_detail' ]);


Route::post('logout', [AuthenticationController::class, 'destroy']);

Route::group([ 'prefix' => 'v1'], function () {
    Route::post('login', [AuthenticationController::class, 'store']);    

    Route::resource('categories', CategoryController::class);
    Route::resource('products', ProductController::class)->middleware('auth:api');
    
    Route::match(['delete'], '/delete_image/{image_id}', [ProductController::class, 'delete_image' ])->middleware('auth:api');
    Route::match(['delete'], '/delete_video/{video_id}', [ProductController::class, 'delete_video' ])->middleware('auth:api');
});



