<!DOCTYPE html>
<html lang="en" ng-app="TiendaApp">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
    <title>happy</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('template/css/font-awesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ url('template/css/vendors/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{ url('template/css/vendors/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ url('template/css/vendors/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{ url('template/css/vendors/feather-icon.css')}}">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ url('template/css/vendors/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ url('template/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{ url('template/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ url('template/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  </head>
  <body>
    <!-- login page start-->
    <div class="container-fluid p-0" ng-controller="authCtrl">
      <div class="row m-0">
        <div class="col-12 p-0">    
          <div class="login-card login-dark">
            <div>
              <div><a class="logo" href="index.html">
                <img class="img-fluid for-light" src="{{ url('imgs/logo.png')}}" alt="looginpage" style="max-width: 50%">
              <img class="img-fluid for-dark" src="{{ url('imgs/logo.png')}}" alt="looginpage"></a></div>
              <div class="login-main"> 
                
                  <h4>Entrar a admin</h4>
                  <p>Ingresa tus credenciales</p>
                  <div class="form-group">
                    <label class="col-form-label">Email </label>
                    <input class="form-control" type="email" required="" placeholder="Test@gmail.com" ng-model="input.email">
                  </div>
                  <div class="form-group">
                    <label class="col-form-label">Contraseña</label>
                    <div class="form-input position-relative">
                      <input class="form-control" type="password" name="login[password]" required="" placeholder="*********" ng-model="input.password">
                      <div class="show-hide"><span class="show">                         </span></div>
                    </div>
                  </div>
                  <div class="form-group mb-0">
                    
                    <div class="text-end mt-3">
                      <button class="btn btn-primary btn-block w-100" ng-click="login()">Entrar</button>
                    </div>
                  </div>
                  
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.2/angular.min.js"></script>
      <!-- latest jquery-->
      <script src="{{ url('template/js/jquery.min.js')}}"></script>
      <!-- Bootstrap js-->
      <script src="{{ url('template/js/bootstrap/bootstrap.bundle.min.js')}}"></script>
      <!-- feather icon js-->
      <script src="{{ url('template/js/icons/feather-icon/feather.min.js')}}"></script>
      <script src="{{ url('template/js/icons/feather-icon/feather-icon.js')}}"></script>
      <!-- scrollbar js-->
      <!-- Sidebar jquery-->
      <script src="{{ url('template/js/config.js')}}"></script>
      <!-- Plugins JS start-->
      <!-- Plugins JS Ends-->
      <!-- Theme js-->
      <script src="{{ url('template/js/script.js')}}"></script>
      <script src="{{ asset('/app/main.js') }}"></script>
      <script src="{{ asset('/app/ctrl/auth_ctrl.js') }}"></script>
      <script src="{{ asset('/app/factory/auth_fact.js') }}"></script>

      <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    </div>
  </body>
</html>