<!DOCTYPE html>
<html lang="en" ng-app="TiendaApp">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Tuin encuentra tu match inmobiliario">
    <meta name="keywords" content="Agentes inmobiliarios, inmuebles,inmobiliario, renta, venta ">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ asset('/imgs/logo.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('/imgs/logo.png') }}" type="image/x-icon">
    <title>happy </title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/font-awesome.css') }}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/icofont.css') }}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/themify.css') }}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/flag-icon.css') }}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/feather-icon.css') }}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/scrollbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/animate.css') }}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/vendors/bootstrap.css') }}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/style.css') }}">
    
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/css/responsive.css') }}">
    <link id="color" rel="stylesheet" href="{{ asset('/template/css/color-1.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    

    <style>
        .landing-page .navbar-nav .nav-item .nav-link{
            color: #000;
        }
    </style>

    @yield('page-styles')

    
  </head>
  <body class="landing-page" >
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="landing-page">
      <!-- Page Body Start            -->
      <div class="landing-home">
        <div class="container-fluid">
          <div class="sticky-header">
            <header style="background:#000000 !important; display:none">                       
              <nav class="navbar navbar-b navbar-dark navbar-trans navbar-expand-xl fixed-top nav-padding" id="sidebar-menu">
                <a class="navbar-brand p-0" href="#"><img class="img-fluid" src="{{ asset('/imgs/logo.png') }}" alt="tuin"></a>
    
                <button class="navbar-toggler navabr_btn-set custom_nav" type="button" data-bs-toggle="collapse" data-bs-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation"><span></span><span></span><span></span></button>
                
                
              </nav>
            </header>
          </div>

          @include('nav_site')

          @yield('content') 
        </div>              
      </div>
      
      {{-- @extends('navs.footer') --}}

    </div>
    
    
    <!-- latest jquery-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.2/angular.min.js"></script>
    <script src="{{ asset('/template/js/jquery.min.js') }}"></script>
    <!-- Bootstrap js-->
    <script src="{{ asset('/template/js/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <!-- feather icon js-->
    <script src="{{ asset('/template/js/icons/feather-icon/feather.min.js') }}"></script>
    <script src="{{ asset('/template/js/icons/feather-icon/feather-icon.js') }}"></script>
    <!-- scrollbar js-->
    <script src="{{ asset('/template/js/scrollbar/simplebar.js') }}"></script>
    
    <!-- Plugins JS start-->
    <script src="{{ asset('/template/js/sidebar-menu.js') }}"></script>
    <script src="{{ asset('/template/js/sidebar-pin.js') }}"></script>
    <script src="{{ asset('/template/js/slick/slick.min.js') }}"></script>
    <script src="{{ asset('/template/js/slick/slick.js') }}"></script>
    <script src="{{ asset('/template/js/header-slick.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->

    <script src="{{ asset('/app/main.js') }}"></script>

    <script src="{{ asset('/app/ctrl/products_ctrl.js') }}"></script>
    <script src="{{ asset('/app/factory/products_fact.js') }}"></script>

    @yield('page-scripts')
    
  </body>
</html>