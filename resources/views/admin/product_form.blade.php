@extends('app_admin')

@section('page-styles')
<style>
    .map-nav{
        height: 50px;
    }
    .div_img{
        border: 1px solid;
        height: 300px;
        display: flex;
        align-items: center; /* Centrar verticalmente */
        justify-content: center;
    }
    .btn-trash{
        color: #f00;
        font-size: 3em;
        position: absolute;
        top: 10px;
        right: 15px;
    }
</style>
@endsection
@section('content')    

@if ($title == 'Agregar nuevo producto')
    <div class="page-body-wrapper p-4" ng-controller="ProductDetailAdminCtrl" >
@else
    <div class="page-body-wrapper p-4" ng-controller="ProductDetailAdminCtrl" ng-init="getProduct({{ $product_id }})">
@endif

    <div class="page-body"> 
        <div class="container-fluid map-nav">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3>{{ $title }}</h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">                                       
                        <svg class="stroke-icon">
                          <use href="{{ url('assets/svg/icon-sprite.svg#stroke-home') }}"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item "><a href="{{ url('dashboard') }}">Productos</a></li>
                    <li class="breadcrumb-item active">{{ $title }}</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>        

        <!-- Container-fluid starts-->
        
        <div class="container-fluid product-wrapper">

            <div class="card">
                <div class="card-body">
                    
                    <div class="row my-3">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">                            
                            <label class="form-label">Nombre</label>
                            <input class="form-control" type="text" id="name" name="name" ng-model="input.name">
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <label class="form-label">Categoría</label>
                            
                            <select class="form-control" id="category_id" name="category_id" ng-model="input.category_id">
                                <option value="">Selecciona una categoría </option>
                                @foreach ($categories as $categoria)
                                    <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>    
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row my-3">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <label class="form-label">Cantidad de producto</label>
                            <input class="form-control" type="number" id="stock" name="stock" ng-model="input.stock">
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <label class="form-label">Precio</label>
                            <input class="form-control" type="text" id="price" name="price" ng-model="input.price">
                        </div>
                    </div>

                    <div class="row my-3">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <label class="form-label">Descripción del producto</label>
                            <textarea class="form-control" id="description" name="description" ng-model="input.description"></textarea>
                        </div>                        
                    </div>

                    <div class="row my-3">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 my-3">
                            <h3>Ingresa Imagenes y un video para tu producto</h3>
                        </div>

                        <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 my-3">
                            <div class="text-center div_img" style="display:none" id="img_preview_1" >
                                <i class="icon-trash btn-trash" ng-click="elimina_img_local(1)" ng-show="file_1_trash === 'local' "></i>
                                
                                <input type="hidden" id="input_img_preview_1">
                                <i class="icon-trash btn-trash" ng-click="elimina_img_api(1)" ng-show="file_1_trash === 'api' " ></i>
                            </div>

                            <div class="text-center div_img" style="" id="img_1" onclick="$('#img_file_1').click()" > Haz clic aquí para subir una imagen</div>
                            <input type="file" id="img_file_1" ng-model="input.file1" style="display:none" onchange="handleFileSelect(1, event)" accept=".jpg, .jpeg, .png, .webp">
                        </div>

                        <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 my-3">
                            <div class="text-center div_img" style="display:none" id="img_preview_2" >
                                
                                    <i class="icon-trash btn-trash" ng-click="elimina_img_local(2)" ng-show="file_2_trash === 'local' "></i>
                                
                                    <input type="hidden" id="input_img_preview_2">
                                    <i class="icon-trash btn-trash" ng-click="elimina_img_api(2)" ng-show="file_2_trash === 'api' " ></i>
                                
                            </div>
                            <div class="text-center div_img" style="" id="img_2" onclick="$('#img_file_2').click()" > Haz clic aquí para subir una imagen</div>
                            <input type="file" id ="img_file_2" ng-model="input.file2" style="display:none" onchange="handleFileSelect(2, event)" accept=".jpg, .jpeg, .png, .webp">
                        </div>

                        <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 my-3">
                            <div class="text-center div_img" style="display:none" id="img_preview_3" >
                                <i class="icon-trash btn-trash" ng-click="elimina_img_local(3)" ng-show="file_3_trash === 'local' "></i>
                                
                                <input type="hidden" id="input_img_preview_3">
                                <i class="icon-trash btn-trash" ng-click="elimina_img_api(3)" ng-show="file_3_trash === 'api' " ></i>
                            </div>

                            <div class="text-center div_img" style=""  id="img_3" onclick="$('#img_file_3').click()" > Haz clic aquí para subir una imagen</div>
                            <input type="file" id="img_file_3" ng-model="input.file3" style="display:none" onchange="handleFileSelect(3, event)" accept=".jpg, .jpeg, .png, .webp">
                        </div>

                        <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 my-3">
                            <div class="text-center div_img" style="display:none" id="img_video" >
                                <i class="icon-trash btn-trash" ng-click="elimina_video_local()" ng-show="file_4_trash === 'local' "></i>
                                
                                <input type="hidden" id="input_video_preview">
                                <i class="icon-trash btn-trash" ng-click="elimina_video_api()" ng-show="file_4_trash === 'api' " ></i>

                                <h3 id="video_name"></h3>
                            </div>
                            <div class="text-center div_img" style="" id="video" onclick="$('#video_file').click()" > Haz clic aquí para subir un video</div>
                            <input type="file" ng-model="input.video" style="display:none" id="video_file" onchange="videoFileSelect( event)" accept=".mp4">
                        </div>
                    </div>

                    <div class="row my-3">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            @if ($title == 'Agregar nuevo producto')
                                <button class="btn btn-primary" ng-click="CreateProduct()">Guardar</button>    
                            @else
                                <button class="btn btn-primary" ng-click="UpdateProduct()">Actualizar</button>
                            @endif
                            
                        </div>
                    </div>

                </div>
            </div>
        
            <!-- Container-fluid Ends-->
      </div>
</div>



@endsection
@section('page-scripts')
<script>
function handleFileSelect(id, event) {
    const input = event.target;
    if (input.files && input.files[0]) {
        const reader = new FileReader();

        reader.onload = function (e) {
            const base64Image = e.target.result;
            
            const profileImage = document.getElementById('img_preview_'+id);

            // Cambia el background a la imagen en base64
            profileImage.style.background = `url(${base64Image})`;
            profileImage.style.backgroundSize = 'cover';
            $('#img_preview_'+id).show();
            $('#img_'+id).hide();

        };

        reader.readAsDataURL(input.files[0]);
    }
}

function videoFileSelect( event) {
    const input = event.target;
    if (input.files && input.files[0]) {
        const reader = new FileReader();

        reader.onload = function (e) {
            const base64Image = e.target.result;
            const fileName = input.files[0].name;
            document.getElementById('video_name').textContent = 'Nombre de video cargado: '+fileName;

            $('#img_video').show();
            $('#video').hide();
            console.log(e.target)
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection