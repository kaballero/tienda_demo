<div class="modal fade" id="delete_product" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h1 class="modal-title fs-5" id="fullScreenModalLabel">¿Seguro que deseas eliminar este producto?</h1>
            <button class="btn-close py-0" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="modal-toggle-wrapper"> 
            <div class="text-sm-center"> <i class="fa fa-trash-o tuin-color" style="font-size:30px" ></i>  </div>
            
            <p class="text-sm-center" > Al eliminar un producto no podrás recuper información </p>
            
          </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-light" type="button" data-bs-dismiss="modal">Cerrar</button>            
                <input type="hidden" name="input_product_id" id="input_product_id" value="" >
                <button class="btn btn-primary" type="submit" ng-click="confirma_eliminar_producto()">Eliminar </button>
            
        </div>
      </div>
    </div>
</div>