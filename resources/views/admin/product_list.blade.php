@extends('app_admin')

@section('page-styles')
<style>
    .map-nav{
        height: 50px;
    }
</style>
@endsection
@section('content')    

<div class="page-body-wrapper p-4" ng-controller="ProductAdminCtrl" ng-init="ProductList()" >
    <div class="page-body"> 
        <div class="container-fluid map-nav">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3>Productos</h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">                                       
                        <svg class="stroke-icon">
                          <use href="../assets/svg/icon-sprite.svg#stroke-home"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item active">Productos</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>        

        <!-- Container-fluid starts-->
        
        <div class="container-fluid product-wrapper">

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-lg-12">
                            <a class="btn btn-primary" href="{{ url('admin/product/add') }}"> Agregar producto</a>
                        </div>
                    </div>

                    <div class="list-product">
                        <table class="table" id="project-status">
                            <thead> 
                                <tr>
                                    <th>Id</th>
                                    <th></th>
                                    <th>Nombre producto</th>
                                    <th>Categoría</th>
                                    <th>Precio</th>
                                    <th>Stock</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="product in products">
                                    <td> {[{ product.id }]}</td>
                                    <td> <img class="img-fluid" src="{{ asset('storage/') }}/{[{ product.images[0].url }]}" alt="{[{ product.images[0].name }]}" style="max-width: 80px">    </td>
                                    <td> {[{ product.name }]}</td>
                                    <td>{[{ product.category }]}</td>
                                    <td> ${[{ product.price }]}MXN </td>
                                    <td> {[{ product.stock}]}</td>
                                    <td>
                                        <ul class="action"> 
                                            <li class=""> <a href="{{ url('admin/product/edit/') }}/{[{ product.id }]}" class="docs-creator"><i class="icon-pencil-alt"></i></a></li>
                                            <li class="delete"><a  class="docs-creator" ng-click="delete_product(product.id)" ><i class="icon-trash"></i></a></li>
                                          </ul>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
            <!-- Container-fluid Ends-->
      </div>
</div>

@include('admin.modal_delete_product')

@endsection