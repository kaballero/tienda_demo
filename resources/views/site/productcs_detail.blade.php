@extends('app')

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('template/css/vendors/owlcarousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/css/vendors/rating.css') }}">
<style>
    
    .map-nav{
        height: 50px;
    }
</style>
@endsection
@section('content')    

<div class="page-body-wrapper p-4" >
    
    <div class="page-body">        
        <div class="container-fluid map-nav">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3>Productos</h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">                                       
                        <svg class="stroke-icon">
                          <use href="../assets/svg/icon-sprite.svg#stroke-home"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item "> <a href="{{ url('/') }}">Productos</a></li>
                    <li class="breadcrumb-item active">Detalle de producto</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>        

        <!-- Container-fluid starts-->
        
        <div class="container-fluid">
            <div>
              <div class="row product-page-main p-0">
                <div class="col-xxl-4 col-md-6 box-col-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="product-slider owl-carousel owl-theme" id="sync1">
                        @foreach($product['images'] as $img )
                            <div class="item" ><img src="{{ asset('storage/'.$img->url) }}" alt=""></div>
                        @endforeach
                      </div>
                      <div class="owl-carousel owl-theme" id="sync2">
                        @foreach($product['images'] as $img )
                            <div class="item" ><img src="{{ asset('storage/'.$img->url) }}" alt=""></div>
                        @endforeach
                        
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xxl-5 box-col-6 order-xxl-0 order-1">
                  <div class="card">
                    <div class="card-body">
                      <div class="product-page-details">
                        <h3>{{ $product['name'] }}</h3>
                      </div>
                      <div class="product-price">${{ $product['price'] }}
                        
                      </div>
                      <ul class="product-color">
                        <li class="bg-primary"></li>
                        <li class="bg-secondary"></li>
                        <li class="bg-success"></li>
                        <li class="bg-info"></li>
                        <li class="bg-warning"></li>
                      </ul>
                      <hr>
                      <p>{{ $product['description']}}</p>
                      <hr>
                      <div>
                        <table class="product-page-width">
                          <tbody>
                            <tr>
                              <td> <b>Brand &nbsp;&nbsp;&nbsp;:</b></td>
                              <td>Pixelstrap</td>
                            </tr>
                            <tr>
                              <td> <b>Availability &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b></td>
                              <td class="txt-success">In stock</td>
                            </tr>
                            <tr>
                              <td> <b>Seller &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b></td>
                              <td>ABC</td>
                            </tr>
                            <tr>
                              <td> <b>Fabric &nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</b></td>
                              <td>Cotton</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-4">
                          <h6 class="product-title">Compartir</h6>
                        </div>
                        <div class="col-md-8">
                          <div class="product-icon">
                            <ul class="product-social">
                              <li class="d-inline-block"><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                              <li class="d-inline-block"><a href="https://accounts.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                              <li class="d-inline-block"><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                              <li class="d-inline-block"><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                              <li class="d-inline-block"><a href="https://rss.app/" target="_blank"><i class="fa fa-rss"></i></a></li>
                            </ul>
                            <form class="d-inline-block f-right"></form>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-4">
                          <h6 class="product-title">Calificación</h6>
                        </div>
                        <div class="col-md-8">
                          <div class="d-flex">
                            <select id="u-rating-fontawesome" name="rating" autocomplete="off">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                            </select><span>(250 review)</span>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="m-t-15 btn-showcase"><a class="btn btn-primary" href="cart.html" title=""> <i class="fa fa-shopping-basket me-1"></i>Agregar</a><a class="btn btn-success" href="checkout.html" title=""> <i class="fa fa-shopping-cart me-1"></i>Buy Now</a><a class="btn btn-secondary" href="list-wish.html"> <i class="fa fa-heart me-1"></i>Add To WishList</a></div>
                    </div>
                  </div>
                </div>
                <div class="col-xxl-3 col-md-6 box-col-6">
                  <div class="card">
                    <div class="card-body">
                      <!-- side-bar colleps block stat-->
                      <div class="filter-block">
                        <h4>Brand</h4>
                        <ul>
                          <li>Clothing</li>
                          <li>Bags</li>
                          <li>Footwear</li>
                          <li>Watches</li>
                          <li>ACCESSORIES</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-body">
                      <div class="collection-filter-block">
                        <ul class="pro-services">
                          <li>
                            <div class="media"><i data-feather="truck"></i>
                              <div class="media-body">
                                <h5>Free Shipping                                    </h5>
                                <p>Free Shipping World Wide</p>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div class="media"><i data-feather="clock"></i>
                              <div class="media-body">
                                <h5>24 X 7 Service                                    </h5>
                                <p>Online Service For New Customer</p>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div class="media"><i data-feather="gift"></i>
                              <div class="media-body">
                                <h5>Festival Offer                                 </h5>
                                <p>New Online Special Festival</p>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div class="media"><i data-feather="credit-card"></i>
                              <div class="media-body">
                                <h5>Online Payment                                  </h5>
                                <p>Contrary To Popular Belief.                                   </p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <!-- silde-bar colleps block end here-->
                  </div>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="row product-page-main">
                <div class="col-sm-12">
                  <ul class="nav nav-tabs border-tab nav-primary mb-0" id="top-tab" role="tablist">
                    
                    <li class="nav-item"><a class="nav-link" id="profile-top-tab" data-bs-toggle="tab" href="#top-profile" role="tab" aria-controls="top-profile" aria-selected="false">Video</a>
                      <div class="material-border"></div>
                    </li>
                    <li class="nav-item"><a class="nav-link" id="contact-top-tab" data-bs-toggle="tab" href="#top-contact" role="tab" aria-controls="top-contact" aria-selected="true">Detalle</a>
                      <div class="material-border"></div>
                    </li>
                    
                  </ul>

                  <div class="tab-content" id="top-tabContent">
                    
                    <div class="tab-pane fade active show" id="top-profile" role="tabpanel" aria-labelledby="profile-top-tab">
                        <video class="embed-responsive-item" controls>
                            <source src="{{ asset('storage/'.$product['video'][0]['url']) }}" type="video/mp4">
                            Tu navegador no soporta el elemento de video.
                        </video>

                        
                    </div>
                    <div class="tab-pane fade" id="top-contact" role="tabpanel" aria-labelledby="contact-top-tab">
                      <p class="mb-0 m-t-20">{{ $product['description'] }}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- Container-fluid Ends-->
      </div>
</div>

@endsection
@section('page-scripts')
<script src="{{ asset('template/js/rating/rating-script.js') }}"></script>
<script src="{{ asset('template/js/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('template/js/ecommerce.js') }}"></script>
    
@endsection