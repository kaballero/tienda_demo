@extends('app')

@section('page-styles')
<style>
    .map-nav{
        height: 50px;
    }
</style>
@endsection
@section('content')    

<div class="page-body-wrapper p-4" ng-controller="ProductsCtrl" >
    <div class="page-body"> 
        <div class="container-fluid map-nav">
            <div class="page-title">
              <div class="row">
                <div class="col-6">
                  <h3>Productos</h3>
                </div>
                <div class="col-6">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">                                       
                        <svg class="stroke-icon">
                          <use href="../assets/svg/icon-sprite.svg#stroke-home"></use>
                        </svg></a></li>
                    <li class="breadcrumb-item active">Productos</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>        

        <!-- Container-fluid starts-->
        
        <div class="container-fluid product-wrapper">
          <div class="product-grid">
            
            <div class="product-wrapper-grid">
              <div class="row">
                <div class="col-xl-3 col-sm-6 xl-4" ng-repeat="product in products">
                  <div class="card">
                    <div class="product-box">
                      <div class="product-img">
                        <img class="img-fluid" src="{{ asset('storage/') }}/{[{ product.images[0].url }]}" alt="">                        
                      </div>
                      
                      <div class="product-details">
                        <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                        <a href="{{ url('product/') }}/{[{ product.title_slug }]}" >
                          <h4>{[{ product.name }]}</h4></a>
                        <p>{[{ product.description }]}</p>
                        <div class="product-price">${[{ product.price }]}
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


        </div>
        <!-- Container-fluid Ends-->
      </div>
</div>

@endsection