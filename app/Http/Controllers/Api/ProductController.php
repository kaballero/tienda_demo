<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

use App\Http\Controllers\Controller;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\ProductVideos;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function index()
    {
        //
        $products = Products::all();
        $response = [];
        foreach($products as $prod){
            $row =  [];
            $row['id'] = $prod->id;
            $row['name'] = $prod->name;
            $row['title_slug'] = $prod->title_slug;
            $row['description'] = $prod->description;
            $row['price'] = $prod->price;
            $row['category_id'] = $prod->category_id;
            $row['category'] = $prod->category->nombre;
            $row['images']= $prod->images;
            $row['video']= $prod->videos;
            $row['stock']= $prod->stock;
            $response[] = $row;
        }
        return response()->json( ['data'=> $response], 200);
    }

    public function list(){
        $products = Products::all();
        
        $response = [];
        foreach($products as $prod){
            $row =  [];
            $row['id'] = $prod->id;
            $row['name'] = $prod->name;
            $row['title_slug'] = $prod->title_slug;
            $row['description'] = $prod->description;
            $row['price'] = $prod->price;
            $row['category_id'] = $prod->category_id;
            $row['category'] = $prod->category->nombre;
            $row['images']= $prod->images;
            $row['video']= $prod->videos;
            $response[] = $row;
        }

        return response()->json( ['data'=> $response], 200);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => ['required'],
            'category_id' => ['required'],
            'stock' => ['required'],
            'price' => ['required'],
            'description' => ['required'],
        ]);
        if (!$request->hasFile('imagen')) {
            $resp=['error'=> 'Debes agregar al menos una imagen '];
            return response()->json( $resp , 421);
        }

        try {
            $originalSlug = Str::slug($request->input('name'));
            $slug = $originalSlug;      
            $counter = 1;
            while (Products::where('title_slug', $slug)->exists()) {
                $slug = $originalSlug . '-' . $counter;
                $counter++;
            }
        
            $product = new Products();
            $product->name = $request->input('name');
            $product->stock = $request->input('stock');
            $product->description = $request->input('description');
            $product->category_id = $request->input('category_id');
            $product->price = $request->input('price');
            $product->title_slug = $slug;
            $product->save();

            $product_id = $product->id;

            if ($request->hasFile('imagen')) {

                foreach($request->file('imagen') as $file_img){
                    $img_path = $this->UploadImage($file_img, $product_id);

                    if($img_path['status'] == 'ok'){
                        $image_path = new ProductImages();
                        $image_path->product_id = $product_id;
                        $image_path->name = $file_img->getClientOriginalName();
                        $image_path->url = $img_path['url'];
                        $image_path->save();
                    }
                }                
            }

            if ($request->hasFile('video')) {
                
                $img_path = $this->UploadVideo($request->file('video'), $product_id);

                if($img_path['status'] == 'ok'){
                    $image_path = new ProductVideos();
                    $image_path->product_id = $product_id;
                    $image_path->name = $request->file('video')->getClientOriginalName();
                    $image_path->url = $img_path['url'];
                    $image_path->save();         
                }
                
            }
            
            return response()->json( 'Producto creado correctamente' , 200);
        } catch (\Throwable $th) {
            return response()->json( 'Ocurrió un problema no se pudo crear el producto', 401);
        }  
    }

    public function UploadImage($file_img, $product_id){
        
        try{
            $folder = 'product_'.$product_id;

            // if (!File::exists($folder)) {
            //     // Crear la carpeta si no existe
            //     File::makeDirectory($folder, 0755, true);
            // }

            // if (!File::exists($folder)) {
            //     // Crear la carpeta si no existe
            //     File::makeDirectory($folder, 0755, true);
            // }

            
            $nombreArchivo = time() . '_' . $file_img->getClientOriginalName();
            $file_img->storeAs($folder, $nombreArchivo, 'public');

            $ruta_file = $folder.'/'.$nombreArchivo;
            return ['status' => 'ok', 'url' => $ruta_file];
            
            // $archivo_background->move($folder, $nombreArchivo);
            // $ruta_archivo =  $folder . '/' . $nombreArchivo;
            // $user->profile_img = $ruta_archivo;
            // $user->save();

        }catch (\Throwable $th) {
            return ['status' => 'fail', 'message' => 'No se pudo guardar la imagen'];
        }
        
    }

    public function UploadVideo($file_img, $product_id){
        
        try{
            $folder = 'video_'.$product_id;

            // if (!File::exists($folder)) {
            //     // Crear la carpeta si no existe
            //     File::makeDirectory($folder, 0755, true);
            // }

            // if (!File::exists($folder)) {
            //     // Crear la carpeta si no existe
            //     File::makeDirectory($folder, 0755, true);
            // }

            
            $nombreArchivo = time() . '_' . $file_img->getClientOriginalName();
            $file_img->storeAs($folder, $nombreArchivo, 'public');

            $ruta_file = $folder.'/'.$nombreArchivo;
            return ['status' => 'ok', 'url' => $ruta_file];
            
            // $archivo_background->move($folder, $nombreArchivo);
            // $ruta_archivo =  $folder . '/' . $nombreArchivo;
            // $user->profile_img = $ruta_archivo;
            // $user->save();

        }catch (\Throwable $th) {
            return ['status' => 'fail', 'message' => 'No se pudo guardar la imagen'];
        }
        
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $product = Products::find($id);   
        $response = [];
        if($product){
            $response['id'] = $product->id;
            $response['name'] = $product->name;
            $response['title_slug'] = $product->id;
            $response['description'] = $product->id;
            $response['price'] = $product->price;
            $response['category_id'] = $product->category_id;
            $response['category'] = $product->category->nombre;
            $response['stock'] = $product->stock;
            $response['images']= $product->images;
            $response['video']= $product->videos;
        }

        return response()->json( ['data'=>$response] , 200);
    }

    public function show_detail(string $slug){
        $product = Products::where('title_slug', $slug)->first();
        $response = [];
        if($product){
            $response['id'] = $product->id;
            $response['name'] = $product->name;
            $response['title_slug'] = $product->id;
            $response['description'] = $product->id;
            $response['price'] = $product->price;
            $response['stock'] = $product->stock;
            $response['category_id'] = $product->category_id;
            $response['category'] = $product->category->nombre;
            $response['images']= $product->images;
            $response['video']= $product->videos;
        }

        return response()->json( ['data'=>$response] , 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        
        try {
            $product = Products::where('id', $id)->first();

            $originalSlug = Str::slug($request->input('name'));
            $slug = $originalSlug;      
            $counter = 1;
            while (Products::where('title_slug', $slug)->exists()) {
                $slug = $originalSlug . '-' . $counter;
                $counter++;
            }
            
            $product->name = $request->input('name');
            $product->stock = $request->input('stock');
            $product->description = $request->input('description');
            $product->category_id = $request->input('category_id');
            $product->price = $request->input('price');
            $product->title_slug = $slug;
            $product->save();

            $product_id = $product->id;
            
            if ($request->hasFile('imagen')) {

                foreach($request->file('imagen') as $file_img){
                    $img_path = $this->UploadImage($file_img, $product_id);
                    
                    if($img_path['status'] == 'ok'){
                        $image_path = new ProductImages();
                        $image_path->product_id = $product_id;
                        $image_path->name = $file_img->getClientOriginalName();
                        $image_path->url = $img_path['url'];
                        $image_path->save();
                    }
                }                
            }

            if ($request->hasFile('video')) {                
                $img_path = $this->UploadVideo($request->file('video'), $product_id);
                if($img_path['status'] == 'ok'){
                    $image_path = new ProductVideos();
                    $image_path->product_id = $product_id;
                    $image_path->name = $request->file('video')->getClientOriginalName();
                    $image_path->url = $img_path['url'];
                    $image_path->save();         
                }                
            }

            return response()->json( 'Producto actualizado correctamente' , 200);
        } catch (\Throwable $th) {
            return response()->json( ['message'=>'Ocurrió un problema no se pudo crear el producto', 'err'=> $th], 401);
        }  

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //Eliminar las imagenes
        ProductImages::where('product_id', $id)->delete();
        //Eliminar los videos
        ProductVideos::where('product_id', $id)->delete();
        
        //Eliminar producto
        Products::where('id', $id)->delete();
        return response()->json( ['message'=>'Producto eliminado correctamente'] , 200);
    }

    public function delete_image($image_id){
        ProductImages::where('id', $image_id)->delete();
        return response()->json( ['data'=>'Imagen del producto eliminado correctamente'] , 200);
    }

    public function delete_video($video_id){
        ProductVideos::where('id', $video_id)->delete();
        return response()->json( ['data'=>'Video del producto eliminado correctamente'] , 200);
    }
}
