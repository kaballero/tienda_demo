<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Middleware\Authenticate;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Authenticate $authMiddleware, Request $request)
    {
        //
        $redirectUrl = $authMiddleware->getRedirectUrl($request);

        $categories = Categories::all();
        return response()->json( ['data'=> $categories], 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        try {
            $cat = new Categories();
            $cat->nombre = $request->input('name');
            $cat->descripcion = $request->input('description');
            $cat->save();

            return response()->json( 'Categoría creada correctamente' , 200);
        } catch (\Throwable $th) {
            return response()->json( 'Ocurrió un problema no se pudo crear la categoría', 401);
        }    
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $category = Categories::find($id);   
        return response()->json( ['data'=> $category], 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        return response()->json( 'Categoría actualizada correctamente' , 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'nullable',
        ]);

        $category = Categories::find($id);

        $category->update([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion,
        ]);

        return response()->json( 'Categoría actualizada correctamente' , 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $category = Categories::find($id);
        $category->delete();
        return response()->json( 'Categoría eliminada correctamente' , 200);
    }
}
