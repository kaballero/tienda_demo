<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{
    //
    public function store(Request $request)
    {   
        $request->validate([
            'email'=> ['required', 'email'],
            'password' => ['required']
        ]);
        
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            // successfull authentication
            
            $user = User::find(Auth::user()->id);

            $token = $user->createToken('TokenApp')->accessToken;
            
            return response()->json([
                'success' => true,
                'token' => $token,
                'user' => $user,
            ], 200);
        } else {
            // failure to authenticate
            return response()->json([
                'success' => false,
                'message' => 'Usuario o contraseña incorrecta',
            ], 401);
        }
    }   

    public function login(){
        return view('login');
    }

    public function registro(Request $request){
        
        $user = new User();
        $user->name = 'Omar';
        $user->email = 'ing.omarkaballero@gmail.com';
        $user->password = Hash::make('admin');
        $user->save();

        Auth::login($user);
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session->invalidate();

    }
}
