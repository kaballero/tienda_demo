<?php

namespace App\Http\Controllers\site;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //

    public function index(){
        
        return view('admin.product_list');
    }

    public function add_product($id = null){
        $param['product_id'] = $id; 
        $param['categories'] = Categories::all();
        $param['title'] = ($id == null)? 'Agregar nuevo producto':  'Actualizar producto';
        return view('admin.product_form', $param);
    }
}
