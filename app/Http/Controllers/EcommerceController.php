<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

class EcommerceController extends Controller
{
    //
    public function index(){
        return view('site/productcs_list');
    }

    public function detail_product($product_slug){
        
        $product = Products::where('title_slug', $product_slug)->first();
        $response = [];
        if($product){
            $response['id'] = $product->id;
            $response['name'] = $product->name;
            $response['title_slug'] = $product->id;
            $response['description'] = $product->description;
            $response['price'] = $product->price;
            $response['category_id'] = $product->category_id;
            $response['category'] = $product->category->nombre;
            $response['images']= $product->images;
            $response['video']= $product->videos;
        }
        $param['product'] = $response;
        
        return view('site/productcs_detail', $param);
    }
}
