var app = angular.module('TiendaApp',[ ]);
app.config(function($interpolateProvider){ $interpolateProvider.startSymbol('{[{').endSymbol('}]}'); });

app.run(function ($rootScope) {
    const protocol = window.location.protocol;
    var host = window.location.host;
    
    $rootScope.apiCnn = protocol+'//'+host;
    if(host === 'localhost:8000/'){
        $rootScope.apiCnn = 'http://localhost:8000';
    }

    $rootScope.logout = function(){
        localStorage.removeItem('UserSesion');
        window.location.href = '/login';
    }
});