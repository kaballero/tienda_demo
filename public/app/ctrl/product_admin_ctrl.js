app.controller('ProductAdminCtrl', function($scope, $rootScope, ProductsAdminApi ){
    
    $scope.session = JSON.parse(localStorage.getItem('UserSesion'));    
    if($scope.session == null){
        $rootScope.logout();
    }
    
    $scope.products = [];
    
    $scope.ProductList = function(){
        
        ProductsAdminApi.getList($scope.session.token).then(resp=>{
            $scope.products = resp.data.data;            
        }, err =>{
            if(err.data.message === "Unauthenticated."){
                $rootScope.logout()
            }
        });
    }

    $scope.delete_product = function(product_id){
        $("#input_product_id").val(product_id)
        $("#delete_product").modal('show')
    }

    $scope.confirma_eliminar_producto = function(){
        product_id = $("#input_product_id").val()
        ProductsAdminApi.deleteProduct(product_id, $scope.session.token).then(resp=>{
            toastr.success('Producto eliminado correctamente');
            $("#input_product_id").val('')
            $("#delete_product").modal('hide')
            $scope.ProductList()            
        }, err =>{
            console.log(err)
            if(err){
                if(err.data.message === "Unauthenticated."){
                    $rootScope.logout()
                }
            }
            
        });
    }

});

app.controller('ProductDetailAdminCtrl', function($scope, $rootScope, ProductsAdminApi,$http ){
    $scope.session = JSON.parse(localStorage.getItem('UserSesion'));    
    if($scope.session == null){
        $rootScope.logout();
    }
    $scope.input = {}; 
    $scope.product_id = '';
    $scope.file_1_trash = 'local'
    $scope.file_2_trash = 'local'
    $scope.file_3_trash = 'local'
    $scope.file_4_trash = 'local'
    

    $scope.getProduct = function(product_id){
        $scope.product_id = product_id;
        ProductsAdminApi.ProductDetail(product_id, $scope.session.token).then(resp=>{
            var product = resp.data.data
            $scope.input.name = product.name;
            $scope.input.category_id = product.category_id.toString();
            $scope.input.stock = product.stock;
            $scope.input.price = product.price;
            $scope.input.description = product.description;            
        
            if(product.video.length >0){
                $scope['file_4_trash'] = 'api'
                document.getElementById('video_name').textContent = 'Nombre de video cargado: '+product.video[0].name;
                $('#input_video_preview').val(product.video[0].id);
                $('#img_video').show();
                $('#video').hide();   
            }

            if(product.images.length > 0){
                for(a=0; a < product.images.length; a++){
                    var div_id = a + 1;
                    var profileImage = document.getElementById('img_preview_'+div_id);

                    var url_img = $rootScope.apiCnn+'/storage/'+product.images[a].url;
                    
                    profileImage.style.background = 'url('+url_img+')';
                    profileImage.style.backgroundSize = 'cover';

                    $scope['file_'+div_id+'_trash'] = 'api'
                    $('#input_img_preview_'+div_id).val(product.images[a].id);
                    $('#img_preview_'+div_id).show();
                    $('#img_'+div_id).hide();
                }
            }

            

        }, err =>{
            if(err.data.message === "Unauthenticated."){
                $rootScope.logout()
            } 
        });
    }

    $scope.CreateProduct = function(){
        var formData = new FormData();
        formData.append('name', ($scope.input.name)?$scope.input.name:'' );
        formData.append('category_id', ($scope.input.category_id)?$scope.input.category_id:'');
        formData.append('stock', ($scope.input.stock)? $scope.input.stock:'');
        formData.append('price', ($scope.input.price)?$scope.input.price:'');
        formData.append('description', ($scope.input.description)? $scope.input.description:'');
        
        const file1 = document.getElementById('img_file_1');
        if (file1.files.length > 0) {
            formData.append('imagen[]', file1.files[0]);
        }
        
        const file2 = document.getElementById('img_file_2');
        if (file2.files.length > 0) {
            formData.append('imagen[]', file2.files[0]);
        }

        const file3 = document.getElementById('img_file_3');
        if (file3.files.length > 0) {
            formData.append('imagen[]', file3.files[0]);
        }

        const video = document.getElementById('video_file');
        if (file3.files.length > 0) {
            formData.append('video', video.files[0]);
        }


        $http({
            method: 'POST',
            url: 'http://localhost:8000/api/v1/products',
            headers: {
                'Content-Type': undefined,  // Deja que el navegador configure el Content-Type
                'Authorization': 'Bearer ' + $scope.session.token
            },
            data: formData,
            transformRequest: angular.identity  // Evita que Angular modifique la solicitud
        }).then(function(response) {
            // Manejar la respuesta
            toastr.success(response.data);
            setTimeout(() => {
                window.location.href = "/dashboard";
            }, 2000);
        }, function(error) {
            // Manejar errores
            
            console.log(error.data.error);
            errorMessage = error.data.errors;
            angular.forEach(errorMessage, function(value, key) {                
                angular.forEach(value, function(errorMessage) {                    
                    toastr.error(errorMessage);                    
                });
            });

            if(error.data.error){
                toastr.error(error.data.error);   
            }
        });

    }

    $scope.UpdateProduct = function(){
        var formData = new FormData();
        formData.append('_method', 'PUT');
        formData.append('name', ($scope.input.name)?$scope.input.name:'' );
        formData.append('category_id', ($scope.input.category_id)?$scope.input.category_id:'');
        formData.append('stock', ($scope.input.stock)? $scope.input.stock:'');
        formData.append('price', ($scope.input.price)?$scope.input.price:'');
        formData.append('description', ($scope.input.description)? $scope.input.description:'');
        
        const file1 = document.getElementById('img_file_1');
        if (file1.files.length > 0) {
            formData.append('imagen[]', file1.files[0]);
        }
        
        const file2 = document.getElementById('img_file_2');
        if (file2.files.length > 0) {
            formData.append('imagen[]', file2.files[0]);
        }

        const file3 = document.getElementById('img_file_3');
        if (file3.files.length > 0) {
            formData.append('imagen[]', file3.files[0]);
        }

        const video = document.getElementById('video_file');
        if (video.files.length > 0) {
            formData.append('video', video.files[0]);
        }

        
        $http({
            method: 'POST',
            url: 'http://localhost:8000/api/v1/products/'+$scope.product_id,
            headers: {
                'Content-Type': undefined,  // Deja que el navegador configure el Content-Type
                'Authorization': 'Bearer ' + $scope.session.token
            },
            data: formData,
            transformRequest: angular.identity  // Evita que Angular modifique la solicitud
        }).then(function(response) {
            // Manejar la respuesta
            toastr.success(response.data);
            setTimeout(() => {
                window.location.href = "/dashboard";
            }, 2000);
        }, function(error) {
            // Manejar errores
            console.log(error.data.error);
            errorMessage = error.data.errors;
            angular.forEach(errorMessage, function(value, key) {                
                angular.forEach(value, function(errorMessage) {                    
                    toastr.error(errorMessage);                    
                });
            });

            if(error.data.error){
                toastr.error(error.data.error);   
            }
        });

    }

    $scope.elimina_img_api = function(id){
        var image_id = $("#input_img_preview_1").val()
        ProductsAdminApi.DeleteImageProduct(image_id, $scope.session.token).then(resp=>{
            $scope['file'+id+'_trash'] = 'local';
            
            $('#img_preview_'+id).hide();
            $('#img_'+id).show();
        }, err=>{
            if(err.data.message === "Unauthenticated."){
                $rootScope.logout()
            } 
        });
    }

    $scope.elimina_img_local = function(id){
        $('#img_file_1').val('');
        $('#img_preview_'+id).hide();
        $('#img_'+id).show();
    }

    $scope.elimina_video_api=function(){
        var video_id = $("#input_video_preview").val()
        ProductsAdminApi.DeleteVideoProduct(video_id, $scope.session.token).then(resp=>{
            $scope['file_4_trash'] = 'local';            
            $('#img_video').hide();
            $('#video').show();   
        }, err=>{
            if(err.data.message === "Unauthenticated."){
                $rootScope.logout()
            } 
        });
    }

    $scope.elimina_video_local = function(id){
        $('#video_file').val('');
        $('#img_video').hide();
        $('#video').show();   
    }
});