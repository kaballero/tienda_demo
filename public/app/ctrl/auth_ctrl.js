app.controller('authCtrl', function($scope, AuthApi){

    $scope.input = {};

    $scope.login = function(){
        console.log($scope.input.email)
        if ($scope.input.email === '' || $scope.input.email === undefined ){
            toastr.error('Debes ingresar tu correo');
            return
        }

        if ($scope.input.password === '' || $scope.input.password === undefined ){
            toastr.error('Debes ingresar tu contraseña');
            return
        }

        var params = {'email': $scope.input.email, 'password': $scope.input.password};

        AuthApi.login(params).then(resp=>{            
            localStorage.setItem('UserSesion', JSON.stringify(resp.data) )
            window.location.href = "/dashboard";
        }, err =>{
            console.log('existe un errror', err)
        });
    }
})