app.controller('ProductsCtrl', function($scope, $rootScope, ProductsApi ){

    $scope.products = [];

    $scope.ProductsList = function(){
        ProductsApi.getList().then(resp=>{
            console.log(resp)
            $scope.products = resp.data.data;
            console.log($scope.products)
        }, err =>{
            console.log('existe un errror', err)
        });
    }

    $scope.ProductsList();
});

app.controller('ProductCtrl', function($scope, $rootScope, ProductsApi ){
    $scope.product = {};
    $scope.ProductDetail = function(slug){
        ProductsApi.getDetail(slug).then(resp=>{
            $scope.product = resp.data.data;
            console.log($scope.product)
        }, err =>{
            console.log('existe un errror', err)
        });
    }
});