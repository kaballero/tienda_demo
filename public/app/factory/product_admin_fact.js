app.factory('ProductsAdminApi',($http, $rootScope)=>{
    return {
        getList: function(token){
            
            return $http({
                method 	: 'GET',
                url 	: 'http://localhost:8000/api/v1/products',
                responseType : 'json',
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            });
        },
        deleteProduct: function (product_id, token){
            return $http({
                method 	: 'DELETE',
                url 	: 'http://localhost:8000/api/v1/products/'+product_id,
                responseType : 'json',
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            });
        },
        ProductDetail: function(product_id, token ){
            return $http({
                method: 'GET',
                url: 'http://localhost:8000/api/v1/products/'+product_id,
                responseType : 'json',
                headers: {
                    'Content-Type': undefined,  // Deja que el navegador configure el Content-Type
                    'Authorization': 'Bearer ' + token
                }
            });
        },
        DeleteImageProduct: function (img_id, token){
            return $http({
                method 	: 'DELETE',
                url 	: 'http://localhost:8000/api/v1/delete_image/'+img_id,
                responseType : 'json',
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            });
        },
        DeleteVideoProduct: function (video_id, token){
            return $http({
                method 	: 'DELETE',
                url 	: 'http://localhost:8000/api/v1/delete_video/'+video_id,
                responseType : 'json',
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            });
        },
        
    }

});