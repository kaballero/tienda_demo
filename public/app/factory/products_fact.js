app.factory('ProductsApi',($http, $rootScope)=>{
    return {
        getList: function(){
            
            return $http({
                method 	: 'GET',
                url 	: 'http://localhost:8000/api/products/list',
                responseType : 'json',
                headers : {
                    'Content-Type': 'application/json'
                }
            });
        },
        getDetail: function(slug){
            return $http({
                method 	: 'GET',
                url 	: 'http://localhost:8000/api/product/'+slug,
                responseType : 'json',
                headers : {
                    'Content-Type': 'application/json'
                }
            });   
        },
        
    }

});