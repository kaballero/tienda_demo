app.factory('AuthApi',($http, $rootScope)=>{
    return {
        login: function(params){
            return $http({
                method 	: 'POST',
                url 	: 'http://localhost:8000/api/v1/login',
                responseType : 'json',
                data:params,
                headers : {
                    'Content-Type': 'application/json'
                }
            });
        },
        
    }

});